// Toggle de navbar
const toggle = document.querySelector('[data-toggle=toggle]');
const navLinks = document.querySelector('[data-toggle=links]');

toggle.addEventListener('click', () => {
   navLinks.classList.toggle('active');
});

// Ocultar navbar al darle click a un nav link
if (screen.width < 1200) {
   navLinks.addEventListener('click', () => {
      navLinks.classList.toggle('active');
   });
}

// Eventos onscroll
let prevScrollpos = window.pageYOffset;

window.onscroll = _=> {
   const nav = document.querySelector('[data-change=onscroll]');
   
   // Ocultar el navbar al hacer scroll hacia abajo
   let currentScrollPos = window.pageYOffset;
   if (prevScrollpos > currentScrollPos) {
      nav.style.top = "0";
   } else {
      nav.style.top = "-35rem";
   }
   prevScrollpos = currentScrollPos;

   // Volver el navbar negro luego de pasar el header
   const header_dist = document.querySelector('header').scrollHeight;
   if ((window.scrollY + 100) > header_dist)
      nav.setAttribute('data-scroll', 'yes');
   else
      nav.setAttribute('data-scroll', 'no')
   ;
};

// Smooth scroll
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
   anchor.addEventListener('click', e => {
      e.preventDefault();

      document.querySelector(anchor.getAttribute('href')).scrollIntoView({
         behavior: 'smooth'
      });
   });
});