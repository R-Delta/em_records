// se selecciona la imagen para cambiarla 'onmouseevent' en la función
let main_img = document.querySelector('[data-slide=img]')

const changeImg = (element, img) => {
   const links = elem => elem.nextElementSibling.style;

   // cambio de imagen
   main_img.style.backgroundImage = 'url(' + img + '.jpg)';

   // Los links no se pueden clickear por una fraccion de seg
   links(element).pointerEvents = 'none'
   setTimeout(_=> {
      links(element).pointerEvents = 'auto';
   }, 300);

   /* se verifica si se quiere cambiar la imagen de fondo y se le da una
   posición basada en data-position */
   if (getComputedStyle(main_img).backgroundSize == 'cover')
   {
      img_resp = 'url(' + img + '-resp.jpg)'
      main_img.style.backgroundPosition = element.getAttribute('data-position');
      if ( element.getAttribute('data-change') != 'no') {
         main_img.style.backgroundImage = img_resp;
      }
   }
   else {
      main_img.style.backgroundImage = img;
      main_img.style.backgroundPosition = '100% center';
   }

   document.querySelectorAll('[data-slide=hover]').forEach((dt, index) => {
      // se tacha solo el texto seleccionado
      dt.style.textDecoration = 'none';
      element.style.textDecoration = 'line-through'

      // se ocultan los links
      links(dt).display = 'none';

      // se coloca el alt específico
      const alt = {
         0: 'banda biosatélite',
         1: 'bohanan',
         2: 'diego cárdenas',
         3: 'banda el otro grupo',
         4: 'banda generica',
         5: 'guga',
         6: 'banda invisibles',
         7: 'laura román',
         8: 'porto solo',
         9: 'virgilio rodriguez'
      }[index];

      main_img.setAttribute('aria-label', alt);
   });
   
   // se muestra el único link
   links(element).display = 'inline';
};